import { checkNewCar, gCarObj} from './info';

function App() {
  return (
    <div>
      <ul>
        {
          gCarObj.map((elem, index) => {
            //let checkNewCar = elem.year >= 2018 ? 'New car' : 'Old car';
            return <li key={index}>{`make: ${elem.make}, vID: ${elem.vID}, ${checkNewCar(elem.year)}`}</li>
          })
        }
      </ul>
    </div>
  );
}

export default App;
